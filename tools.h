//
// Created by Gregpack on 25-Nov-18.
//

#ifndef DRAWER_TOOLS_H
#define DRAWER_TOOLS_H

#include "Shapes.h"
#include "ShapeFactory.h"
#include <iostream>
void parseanddraw(std::istream &);
template<typename T>
std::string printNames(T figure){
    std::string name = static_cast<std::string>(figure);
    return name;
}

template<typename T, typename... Args>
std::string printNames(T name, Args... args){
    std::string ret = static_cast<std::string>(name);
    return ret + " " + printNames(args...);
}

template<typename T, typename... Args>
double totalArea(T figure, Args... args){
    double ret = totalArea(figure);
    return ret + totalArea (args...);
}

template<typename T>
double totalArea(T figure) {
    double nmb = static_cast<double>(figure);
    return nmb;
}


#endif //DRAWER_TOOLS_H
