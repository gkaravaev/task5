//
// Created by Gregpack on 26-Nov-18.
//

#include "ShapeFactory.h"

void ShapeFactory::reg(const std::string & shapeName,
        std::function<std::unique_ptr<Shape>(std::vector<int> &)> foo) {
    sMap.insert(std::make_pair(shapeName, std::move(foo)));
}

std::unique_ptr<Shape> ShapeFactory::CreateShape(const std::string & name, std::vector <int> & params) {
    auto it = sMap.find(name);
    if (it != sMap.end())
        return std::move(it->second(params));
    else return nullptr;
}
