cmake_minimum_required(VERSION 3.1.0)
project(drawer)

set(CMAKE_CXX_STANDARD 11)

set(SOURCE_FILES bitmap_image.hpp catch.hpp Shape.h Shape.cpp Parameters.cpp Parameters.h Shapes.cpp Shapes.h tools.cpp tools.h ShapeFactory.cpp ShapeFactory.h)
add_executable(drawer ${SOURCE_FILES} main.cpp)
add_executable(unittest tests.cpp ${SOURCE_FILES})