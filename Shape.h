//
// Created by Gregpack on 18-Nov-18.
//

#ifndef DRAWER_SHAPE_H
#define DRAWER_SHAPE_H

#include "Parameters.h"
#include <vector>
#include "bitmap_image.hpp"
#include <memory>

class Segment {
private:
    std::pair <int, int> start;
    std::pair <int, int> end;
public:
    Segment(int, int, int, int);
    std::pair <int, int> getStart () const;
    std::pair <int, int> getEnd() const;
};

class Shape : public Named, public Colored {
protected:
    std::pair <int, int> center;
public:
    virtual ~Shape() = default;
    virtual void setCenter (int, int) = 0;
    virtual double square () const = 0;
    virtual std::vector <Segment> getSegments () const = 0;
    explicit operator double() const;
    void drawShape (bitmap_image &);
    std::string getName() const;
};



#endif //DRAWER_SHAPE_H
