//
// Created by Gregpack on 18-Nov-18.
//

#ifndef DRAWER_COLORED_H
#define DRAWER_COLORED_H


#include <tuple>
#include <string>
#include <map>

class Named {
private:
    static std::map <const std::string, int> amount;
protected:
    static std::string generateName(const std::string &);
    std::string name;
public:
    virtual ~Named() = default;
    explicit operator std::string() const;
    explicit Named() = default;
    explicit Named(const std::string &);
};



class Colored {
private:
    int red;
    int green;
    int blue;
public:
    virtual ~Colored() = default;
    void setColor (int, int, int);
    void setColor (const std::string &);
    std::tuple <int, int, int> getColor();
};


#endif //DRAWER_COLORED_H
