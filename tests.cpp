#define CATCH_CONFIG_MAIN

#include "catch.hpp"
#include "tools.h"
#include <sstream>

TEST_CASE ("Print names tool") {
    class Rectangle rec1 (10, 5), rec2 (10, 10), rec3 (20, 15);
    Circle c1 (10), c2(20), c3(30);
    Triangle t1 (10, 10, 20, 20, 30, 30), t2(20, 12, 12, 4, 1, 2), t3(1, 2, 3, 4, 5, 6);
    for (int i = 0; i < 7; i++) {
        std::stringstream output;
        switch (i) {
            case 0 :
                output << printNames(rec1);
                REQUIRE (output.str() == "Rectangle.1");
                break;
            case 1 :
                output << printNames(c2);
                REQUIRE (output.str() == "Circle.2");
                break;
            case 2 :
                output << printNames(t3);
                REQUIRE (output.str() == "Triangle.3");
                break;
            case 3 :
                output << printNames(rec2, c1);
                REQUIRE (output.str() == "Rectangle.2 Circle.1");
                break;
            case 4 :
                output << printNames(rec3, t1);
                REQUIRE (output.str() == "Rectangle.3 Triangle.1");
                break;
            case 5 :
                output << printNames(c3, t2);
                REQUIRE (output.str() == "Circle.3 Triangle.2");
                break;
            case 6 :
                output << printNames(rec1, c1, t1, Named("Unknown"), Named("Figure"));
                REQUIRE (output.str() == "Rectangle.1 Circle.1 Triangle.1 Unknown.1 Figure.1");
                break;
        }
    }
}

TEST_CASE ("Constructors, getters, setters") {
    Triangle t (0, 0, 10, 0, 0, 10);
    t.setColor("green");
    t.setCenter(50, 50);
    std::tuple <int, int, int> color1 = t.getColor();
    REQUIRE(std::get <1> (color1) == 255);
    REQUIRE(50 == t.square());
    REQUIRE(t.getName() == "Triangle.4");
    Circle c (10);
    c.setColor("red");
    c.setCenter(51, 50);
    std::tuple <int, int, int> color2 = c.getColor();
    REQUIRE(std::get <0> (color2) == 255);
    REQUIRE(314 == c.square());
    REQUIRE(c.getName() == "Circle.4");
    class Rectangle rec (10, 10);
    rec.setColor("blue");
    rec.setCenter(52, 50);
    std::tuple <int, int, int> color3 = rec.getColor();
    REQUIRE(std::get <2> (color3) == 255);
    REQUIRE(100 == rec.square());
    REQUIRE(rec.getName() == "Rectangle.4");
}

TEST_CASE ("Total area tool") {
    class Rectangle rec1 (10, 5), rec2 (10, 10), rec3 (20, 15);
    REQUIRE (static_cast<int>(0.5 + totalArea(rec1, Triangle(0, 0, 0, 6, 6, 6))) == 68);
    REQUIRE (static_cast<int>(totalArea(rec2, Circle(10))) == 414);
    REQUIRE (static_cast<int>(totalArea(rec3, rec2)) == 400);
    REQUIRE (static_cast<int>(totalArea(rec2, Circle(10), Triangle (0, 0, 10, 0, 0, 20))) == 514);
}

TEST_CASE ("Parse and draw"){
    std::ifstream test;
    test.open("../build/test/test.txt");
    parseanddraw(test);
}

