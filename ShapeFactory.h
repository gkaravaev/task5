//
// Created by Gregpack on 26-Nov-18.
//

#ifndef DRAWER_SHAPEFACTORY_H
#define DRAWER_SHAPEFACTORY_H

#include "Shape.h"
#include <map>
#include <memory>
#include <functional>

class ShapeFactory {
private:
    typedef std::map <std::string, std::function<std::unique_ptr <Shape>(std::vector <int> &) > > shapeMap;
    shapeMap sMap;
public:
    void reg (const std::string &, std::function <std::unique_ptr <Shape>(std::vector <int> &)> );
    std::unique_ptr<Shape> CreateShape(const std::string&, std::vector<int> &);

};


#endif //DRAWER_SHAPEFACTORY_H
