//
// Created by Gregpack on 18-Nov-18.
//

#include "Shape.h"

Segment::Segment(int xStart, int yStart, int xEnd, int yEnd)
        :start(std::make_pair (xStart, yStart)), end(std::make_pair (xEnd, yEnd)){}

std::pair<int, int> Segment::getStart() const{
    return start;
}

std::pair<int, int> Segment::getEnd() const{
    return end;
}

void Shape::drawShape(bitmap_image &image) {
        image_drawer draw (image);
        draw.pen_width(2);
        draw.pen_color(std::get<0>(getColor()), std::get<1>(getColor()), std::get<2>(getColor()));
        std::vector <Segment> circ = getSegments();
        for (auto &i : circ) {
            draw.line_segment(i.getStart().first, i.getStart().second, i.getEnd().first, i.getEnd().second);
        }
}

void Shape::setCenter(int x, int y) {
    center = std::make_pair(x, y);
}


std::string Shape::getName() const {
    return name;
}

Shape::operator double() const {
    return square();
}





