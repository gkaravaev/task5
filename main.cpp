#include <iostream>
#include <fstream>
#include "tools.h"

int main(int argc, char **argv) {
    if (argc > 1) {
        std::ifstream f;
        f.open(argv[1]);
        parseanddraw(f);
    } else parseanddraw(std::cin);
    return 0;
}