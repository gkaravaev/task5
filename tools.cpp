//
// Created by Gregpack on 25-Nov-18.
//

#include "tools.h"

void parseanddraw(std::istream & input) {
    std::string temp;
    std::getline (input, temp);
    size_t xPos = temp.find('x');
    std::string temp1 = (temp.substr(xPos + 1, temp.length() - xPos));
    temp = temp.substr(0, xPos);
    ShapeFactory factory;
    factory.reg("Circle", &Circle::ShapeCreator);
    factory.reg("Rectangle", &Rectangle::ShapeCreator);
    factory.reg("Triangle", &Triangle::ShapeCreator);
    std::vector <std::unique_ptr<Shape>> shapes;
    bitmap_image image(atoi(temp.c_str()), atoi(temp1.c_str()));
    int j = 0;
    while (std::getline (input, temp)) {
        std::string name;
        std::vector <int> params;
        int param = 0;
        int i = 0;
        while (temp[i] != '(') { // считать имя
            name += temp[i];
            i++;
        }
        do { //считать параметры
            i++;
            if (temp[i] == ',' || temp[i] == ')') {
                params.push_back(param);
                param = 0;
            }
            else if (temp[i] == ' ')
                continue;
            else {
                param = param * 10 + (temp[i] - '0');
            }
        } while (temp[i] != ')');
        shapes.push_back(factory.CreateShape(name, params));
        params.clear();
        i += 2;
        do { //считать центр
            i++;
            if (temp[i] == ',' || temp[i] == ']') {
                params.push_back(param);
                param = 0;
            }
            else if (temp[i] == ' ')
                continue;
            else {
                param = param * 10 + (temp[i] - '0');
            }
        } while (temp[i] != ']');
        shapes[j]->setCenter(params[0], params[1]);
        params.clear();
        i += 3;
        name = "";
        if (!isdigit(temp[i])) {
            while (temp[i] != '}') { // считать имя
                name += temp[i];
                i++;
            }
            shapes[j]->setColor(name);
        }
        else{
            i--;
            do { //считать цвет
                i++;
                if (temp[i] == ',' || temp[i] == '}') {
                    params.push_back(param);
                    param = 0;
                }
                else if (temp[i] == ' ')
                    continue;
                else {
                    param = param * 10 + (temp[i] - '0');
                }
            } while (temp[i] != '}');
            shapes[j]->setColor(params[0], params[1], params[2]);
        }
        shapes[j]->drawShape(image);
        j++;
    }
    image.save_image("image.bmp");
}


