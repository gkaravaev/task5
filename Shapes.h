//
// Created by Gregpack on 18-Nov-18.
//

#ifndef DRAWER_SHAPES_H
#define DRAWER_SHAPES_H

#include "Shape.h"
#include <cmath>
#include <memory>


class Circle : public Shape {
private:
    int r;
public:
    void setCenter (int, int) override;
    explicit Circle(std::vector <int>);
    explicit Circle(int);
    double square () const override;
    std::vector <Segment> getSegments () const override;
    static std::unique_ptr <Shape> ShapeCreator (std::vector <int> &);

};
class Triangle : public Shape {
private:
    std::pair <int, int> c2;
    std::pair <int, int> c3;
public:
    explicit Triangle (std::vector <int>);
    Triangle (int, int, int, int, int, int);
    void setCenter (int, int) override;
    double square () const override;
    std::vector <Segment> getSegments () const override;
    static std::unique_ptr <Shape> ShapeCreator (std::vector <int> &);

};
class Rectangle : public Shape {
private:
    int height;
    int width;
public:
    explicit Rectangle (std::vector <int>);
    Rectangle (int, int);
    void setCenter (int, int) override;
    double square () const override;
    std::vector <Segment> getSegments () const override;
    static std::unique_ptr <Shape> ShapeCreator (std::vector <int> &);

};
#endif //DRAWER_SHAPES_H
