//
// Created by Gregpack on 18-Nov-18.
//

#include "Shapes.h"

Circle::Circle(std::vector<int> radius) : r (radius[0]){
    name = Named::generateName("Circle");
    setColor(0, 0, 0);
}


double Circle::square() const {
    return r * r * 3.14;
}

std::vector<Segment> Circle::getSegments() const {
    std::vector <Segment> out;
    int x = center.first;
    int y = center.second;
    double dx1, dy1, dx2, dy2;
    dy1 = r / 2.0;
    dx1 = r * sqrt (3) / 2;
    dx2 = dy1;
    dy2 = dx1;
    out.emplace_back(x, y - r, x + (int)dx2, y - (int)dy2);
    out.emplace_back(x + r, y, x + (int)dx1, y - (int)dy1);
    out.emplace_back(x + (int)dx2, y - (int)dy2, x + (int)dx1, y - (int)dy1);
    out.emplace_back(x, y + r, x - (int)dx2, y + (int)dy2);
    out.emplace_back(x - r, y, x - (int)dx1, y + (int)dy1);
    out.emplace_back(x - (int)dx2, y + (int)dy2, x - (int)dx1, y + (int)dy1);
    out.emplace_back(x, y + r, x + (int)dx2, y + (int)dy2);
    out.emplace_back(x + r, y, x + (int)dx1, y + (int)dy1);
    out.emplace_back(x + (int)dx2, y + (int)dy2, x + (int)dx1, y + (int)dy1);
    out.emplace_back(x, y - r, x - (int)dx2, y - (int)dy2);
    out.emplace_back(x - r, y, x - (int)dx1, y - (int)dy1);
    out.emplace_back(x - (int)dx2, y - (int)dy2, x - (int)dx1, y - (int)dy1);
    return out;
}

void Circle::setCenter(int x, int y) {
    center = std::make_pair(x, y);
}

std::unique_ptr<Shape> Circle::ShapeCreator(std::vector<int> & params) {
    std::unique_ptr <Shape> ret = std::unique_ptr <Shape>(new Circle(params));
    return ret;
}

Circle::Circle(int r) : r (r){
    name = Named::generateName("Circle");
    setColor(0, 0, 0);
}


Triangle::Triangle(std::vector <int> coords) {
    center = std::make_pair(0, 0);
    c2 = std::make_pair(center.first + coords[0], center.second + coords[1]);
    c3 = std::make_pair(center.first + coords[2], center.second + coords[3]);
    name = Named::generateName("Triangle");
    setColor(0, 0, 0);
}

void Triangle::setCenter(int x, int y) {
    int dx1, dy1, dx2, dy2;
    dx1 = c2.first - center.first;
    dy1 = c2.second - center.second;
    dx2 = c3.first - center.first;
    dy2 = c3.second - center.second;
    center = std::make_pair (x, y);
    c2 = std::make_pair(center.first + dx1, center.second + dy1);
    c3 = std::make_pair(center.first + dx2, center.second + dy2);

}

std::vector<Segment> Triangle::getSegments() const {
    int x = center.first;
    int y = center.second;
    Segment seg1 (x, y, c2.first, c2.second);
    Segment seg2 (x, y, c3.first, c3.second);
    Segment seg3 (c3.first, c3.second, c2.first, c2.second);
    std::vector <Segment> out;
    out.push_back(seg1);
    out.push_back(seg2);
    out.push_back(seg3);
    return out;
}

double Triangle::square() const {
    double p, a, b, c;
    int x = center.first;
    int y = center.second;
    a = sqrt((x - c2.first) * (x - c2.first) + (y - c2.second) * (y - c2.second));
    b = sqrt((c2.first - c3.first) * (c2.first - c3.first) + (c2.second - c3.second) * (c2.second - c3.second));
    c = sqrt((x - c3.first) * (x - c3.first) + (y - c3.second) * (y - c3.second));
    p = (a + b + c) / 2.0;
    return sqrt (p * (p - a) * (p - b) * (p - c));
}

std::unique_ptr<Shape> Triangle::ShapeCreator(std::vector<int> & params) {
    std::unique_ptr <Shape> ret = std::unique_ptr <Shape>(new Triangle(params));
    return ret;
}

Triangle::Triangle(int x0, int y0, int x1, int y1, int x2, int y2) {
    center = std::make_pair(x0, y0);
    c2 = std::make_pair(center.first + x1, center.second + y1);
    c3 = std::make_pair(center.first + x2, center.second + y2);
    name = Named::generateName("Triangle");
    setColor(0, 0, 0);
}


Rectangle::Rectangle (std::vector <int> lw)
        : height(lw[0]), width(lw[1]) {
    center = std::make_pair(0, 0);
    name = Named::generateName("Rectangle");
    setColor(0, 0, 0);
}

double Rectangle::square() const {
    return width * height;
}

std::vector<Segment> Rectangle::getSegments() const {
    Segment seg1 (center.first + height / 2, center.second + width / 2, center.first - height / 2, center.second + width / 2);
    Segment seg2 (center.first + height / 2, center.second - width / 2, center.first + height / 2, center.second + width / 2);
    Segment seg3 (center.first + height / 2, center.second - width / 2, center.first - height / 2, center.second - width / 2);
    Segment seg4 (center.first - height / 2, center.second + width / 2, center.first - height / 2, center.second - width / 2);
    std::vector <Segment> out;
    out.push_back(seg1);
    out.push_back(seg2);
    out.push_back(seg3);
    out.push_back(seg4);
    return out;

}

void Rectangle::setCenter(int x, int y) {
    center = std::make_pair(x, y);
}

std::unique_ptr<Shape> Rectangle::ShapeCreator(std::vector<int> & params) {
    std::unique_ptr <Shape> ret = std::unique_ptr <Shape>(new Rectangle(params));
    return ret;
}

Rectangle::Rectangle(int h, int w) : height(h), width(w) {
    center = std::make_pair(0, 0);
    name = Named::generateName("Rectangle");
    setColor(0, 0, 0);

}
