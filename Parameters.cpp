//
// Created by Gregpack on 18-Nov-18.
//

#include "Parameters.h"

void Colored::setColor(int r, int g, int b) {
    red = r;
    green = g;
    blue = b;
}

void Colored::setColor(const std::string & colorName) {
    if (colorName == "grey"){
        setColor(84, 84, 84);
        return;
    }
    else if (colorName == "black"){
        setColor(0, 0, 0);
        return;
    }
    else if (colorName == "blue"){
        setColor(0, 0, 255);
        return;
    }
    else if (colorName == "brown"){
        setColor(165, 42, 42);
        return;
    }
    else if (colorName == "green"){
        setColor(0, 255, 0);
        return;
    }
    else if (colorName == "orange"){
        setColor(255, 165, 0);
        return;
    }
    else if (colorName == "pink"){
        setColor(255, 192, 203);
        return;
    }
    else if (colorName == "red"){
        setColor(255, 0, 0);
        return;
    }
    else if (colorName == "purple"){
        setColor(160, 32, 240);
        return;
    }
    else if (colorName == "white"){
        setColor(255, 255, 255);
        return;
    }
    else if (colorName == "yellow"){
        setColor(255, 255, 0);
        return;
    }
    else {
        setColor(0, 0, 0);
        return;
    }
}

std::tuple<int, int, int> Colored::getColor() {
    return std::make_tuple(red, green, blue);
}

std::map <const std::string, int> Named::amount;

std::string Named::generateName(const std::string & figType) {
    if (amount[figType] == 0) amount[figType] = 1;
    std::string answer = figType + "." + std::to_string(amount[figType]);
    amount[figType]++;
    return answer;
}

Named::Named(const std::string & name1) {
    name = Named::generateName(name1);

}

Named::operator std::string() const {
    return name;
}
